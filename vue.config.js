module.exports = {
    css: {
        sourceMap: process.env.NODE_ENV !== 'production',
        loaderOptions: {
            scss: {
                additionalData: `@import "~@/assets/styles/scss/_variables.scss";`,
            },
        },
    },
}
