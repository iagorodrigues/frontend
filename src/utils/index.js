export const CURRENCY_ENUM = {
    EUR: '€',
    USD: '$',
}

export const getCurrencyTypeSymbol = currencyType => CURRENCY_ENUM[currencyType]
