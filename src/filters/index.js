export const currency = (value, currency = 'EUR', locale = 'en-US') => {
    const number = Number(value) || 0
    return number.toLocaleString(locale, {
        style: 'currency',
        currency,
    })
}
