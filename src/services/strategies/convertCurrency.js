import { apolloClient } from '@/vue-apollo'
// import gql from 'graphql-tag'
import ConvertCurrencyQuery from '../queries/ConvertToBRL.gql'

async function convertCurrency(amount = 0, baseCurrency = 'EUR') {
    const [today] = new Date().toISOString().split('T')

    return await apolloClient.query({
        query: ConvertCurrencyQuery,
        variables: {
            amount,
            baseCurrency,
            date: today,
        },
    })
}

export default {
    convertCurrency,
}
