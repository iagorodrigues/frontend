import * as filters from '@/filters'

export default function (Vue) {
    Object.keys(filters).forEach(key => {
        Vue.filter(key, filters[key])
    })

    Vue.prototype.$filters = filters
}
