import DefaultLayout from '@/layout/default'

export default function (Vue) {
    Vue.component('DefaultLayout', DefaultLayout)
}
