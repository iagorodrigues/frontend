export default {
    tipAmount(state) {
        return state.moneyAmount * (state.tipPercent / 100)
    },

    totalAmount(state, getters) {
        return state.moneyAmount + getters.tipAmount
    },

    totalPerPerson(state, getters) {
        return getters.totalAmount / state.people
    },
}
