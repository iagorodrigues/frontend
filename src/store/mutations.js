export default {
    setMoneyAmount: (state, amount) => (state.moneyAmount = amount),
    setTipPercent: (state, tip) => (state.tipPercent = tip),
    setPeople: (state, people) => (state.people = people),
    setCurrencyType: (state, currency) => (state.currencyType = currency),
    setTotalInReal: (state, totalInReal) => (state.totalInReal = totalInReal),
}
