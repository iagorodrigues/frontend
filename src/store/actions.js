import api from '@/services/strategies/convertCurrency'

export default {
    async convertToBRL({ state, commit, getters }) {
        try {
            const { data } = await api.convertCurrency(
                getters.totalAmount,
                state.currencyType
            )

            commit('setTotalInReal', data?.convert[0].quoteAmount ?? 0)
        } catch (error) {
            commit('setTotalInReal', 0)
            console.error('Error trying to convert value', error.message)
        }
    },
}
