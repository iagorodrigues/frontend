export default {
    currencyType: 'EUR',
    totalInReal: 0,
    moneyAmount: 0,
    tipPercent: 10,
    people: 2,
}
