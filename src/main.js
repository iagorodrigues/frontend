import { apolloProvider } from './vue-apollo'
import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import store from './store'

import layouts from './plugins/layouts'
import filters from './plugins/filters'

import './assets/styles/scss/main.scss'

Vue.use(layouts)
Vue.use(filters)

Vue.config.productionTip = false

new Vue({
    store,
    apolloProvider,
    render: h => h(App),
}).$mount('#app')
